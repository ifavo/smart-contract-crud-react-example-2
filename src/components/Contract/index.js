import Definition from './CRUD.json'
const ContractAddress = Definition.networks['5777'].address
const Contract = {}

Definition.abi.forEach(method => {
  if (!method.name || method.type !== 'function') {
    return
  }

  if (method.constant) {
    Contract[method.name] = defineConstant(method)
  } else {
    Contract[method.name] = defineSignedRequest(method)
  }
})

function defineConstant (method) {
  return (...args) => window.connex.thor.account(ContractAddress).method(method).call(...args)
}

function defineSignedRequest (method) {
  return async (...args) => {
    const signingService = window.connex.vendor.sign('tx')
    const transferClause = window.connex.thor.account(ContractAddress).method(method).asClause(...args)

    const transactionInfo = await signingService
      .gas(3000000)
      .request([transferClause])

    return transactionInfo
  }
}

export default Contract
