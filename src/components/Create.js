import { useState } from 'react'
import Contract from './Contract'
import { Form, Input, Button, Typography } from 'antd'
const { Text } = Typography

export default function Create ({ onCreate }) {
  const [key, setKey] = useState('')
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)

  const waitForNextBlock = () => window.connex.thor.ticker().next()
  const getTransactionReceipt = (id) => window.connex.thor.transaction(id).getReceipt()
  const handleChange = (setState) => (e) => setState(e.target.value)
  const waitForTransactionReceipt = async (id) => {
    do {
      await waitForNextBlock()
      const transaction = await getTransactionReceipt(id)
      if (transaction) {
        return transaction
      }
    } while (true)
  }

  const createKey = async () => {
    await setError()
    await setLoading(true)

    try {
      const { txid } = await Contract.create(key, value)
      const transaction = await waitForTransactionReceipt(txid)
      if (transaction.reverted) {
        throw new Error('Transaction was reverted')
      }
      await onCreate()
    } catch (err) {
      await setError(err.message)
    }

    await setLoading(false)
  }

  return (
    <>
      <Form layout='inline'>
        <Form.Item name='key' label='Key'>
          <Input type='text' value={key} onChange={handleChange(setKey)} />
        </Form.Item>
        <Form.Item name='value' label='Value'>
          <Input type='text' value={value} onChange={handleChange(setValue)} />
        </Form.Item>
        <Button onClick={createKey} loading={loading} type='primary'>create</Button>
        {!!error && <Text type='danger'>{error}</Text>}
      </Form>
    </>
  )
}
