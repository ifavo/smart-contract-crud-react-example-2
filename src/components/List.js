import { useState, useEffect } from 'react'
import { Table } from 'antd'
import Create from './Create'
import 'antd/dist/antd.css'

import Contract from './Contract'

export default function List () {
  const [list, setList] = useState([])
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)
  const columns = [
    { title: 'Key', dataIndex: 'key' },
    { title: 'Value', dataIndex: 'value' }
  ]

  const loadList = async () => {
    await setError('')
    await setLoading(true)
    try {
      const data = await Contract.list()
      const list = data.decoded['0'].map((key, index) => ({
        key,
        value: data.decoded['1'][index]
      }))
      await setList(list)
    } catch (err) {
      await setError(err.message)
    }
    await setLoading(false)
  }

  useEffect(() => {
    loadList()
  }, [])

  return (
    <>
      <Create onCreate={loadList} />
      {error}
      <Table columns={columns} dataSource={list} loading={loading} />
    </>
  )
}
